//** Components

var ui = require('./ui');
var medium = require('./medium');

window.onload = function() {
	ui.init();
	medium.init();
};

