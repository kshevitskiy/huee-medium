var UTILITIES = {

    dateFormat: function(date, format) {
        let m = moment(date).format(format);
        return m;
    },

    textFormat: function(text) {
        var content = text.replace(/<img[^>]*>/g,""),
            maxLength = 200,
            trimmedContent = content.substr(0, maxLength);

        trimmedContent = trimmedContent.substr(0, Math.min(trimmedContent.length, trimmedContent.lastIndexOf(' ')));
        return trimmedContent;
    },

    chunkArray: function(myArray, chunkSize) {
        /**
         * Returns an array with arrays of the given size.
         *
         * @param myArray {Array} array to split
         * @param chunkSize {Integer} Size of every group
         */
        var index = 0;
        var arrayLength = myArray.length;
        var tempArray = [];
        
        for (index = 0; index < arrayLength; index += chunkSize) {
            let myChunk = myArray.slice(index, index + chunkSize);
            // Do something if you want with the group            
            tempArray.push(myChunk);
        }
        return tempArray;
    },

	trimText: function(str, length, ending) {

	    if (length == null) {
	    	length = 100;
	    }

	    if (ending == null) {
	    	ending = '...';
	    }

	    if (str.length > length) {
			let trimmedString = str.substr(0, length);
				trimmedString = trimmedString.substr(0, Math.min(trimmedString.length, trimmedString.lastIndexOf(' ')));
	    	return trimmedString + ending;
	    } else {
	    	return str;
	    }
	},    
};

module.exports = {
    dateFormat : UTILITIES.dateFormat,
    textFormat : UTILITIES.textFormat,
    chunkArray : UTILITIES.chunkArray,
    trimText   : UTILITIES.trimText,
};
