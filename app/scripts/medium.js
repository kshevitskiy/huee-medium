//** Import utilities
const util = require('./utilities');

const mediumWrapper = document.getElementById('medium');

//** Slider markup
const swiperWrapper = document.createElement('div');
      swiperWrapper.setAttribute('class', 'swiper-wrapper');
const nextSlide = document.createElement('button');
      nextSlide.setAttribute('class', 'medium-control next-medium-slide');
      nextSlide.innerHTML = '<span class="arrow arrow-right"></span>';
const prevSlide = document.createElement('button');
      prevSlide.setAttribute('class', 'medium-control prev-medium-slide');
      prevSlide.innerHTML = '<span class="arrow arrow-left"></span>';
const pagination = document.createElement('div');
      pagination.setAttribute('class', 'medium-pagination swiper-pagination');

const url = 'https://api.rss2json.com/v1/api.json?rss_url=https://medium.com/feed/@huee';
let slideItem = '';
let postRender = '';

var MEDIUM = {

    getImageUrl: function(source) {
        let tagIndex = source.indexOf('<img'),
            srcIndex = source.substring(tagIndex).indexOf('src=') + tagIndex,
            srcStart = srcIndex + 5,
            srcEnd   = source.substring(srcStart).indexOf('"') + srcStart,
            url      = source.substring(srcStart, srcEnd);
        return url;
    },

    posts: function() {       
        
        var xhr = new XMLHttpRequest();
        xhr.onload = function () {    
            if (xhr.readyState === xhr.DONE) {
                if (xhr.status === 200) {
                    let posts = xhr.response.items;
                    let slides = util.chunkArray(posts, 6);

                    // Slide
                    slides.some( function(slide, i) {

                        // Render slide
                        slideItem = document.createElement('div');
                        slideItem.setAttribute('id', 'slide-' + i +'');
                        slideItem.setAttribute('class', 'slide swiper-slide');
                        
                        for(var post = 0; post < slide.length; post++) {                            
                            let date = util.dateFormat(slide[post].pubDate, 'DD.MM.YYYY'),
                                author = slide[post].author,
                                link = slide[post].link,
                                title = slide[post].title,
                                description = slide[post].description,
                                imgUrl = MEDIUM.getImageUrl(description),
                                copy = util.textFormat(slide[post].description),
                                shortTitle = util.trimText(title, 40);


                            let postMarkup =    '<a href="'+ link + '" target="_blank" title="'+ title +'" class="NewsItem NewsItem_Text OutboundLink">' +
                                                    '<div>' +
                                                        '<span class="Date">' + date + ' by ' + author + '</span>' +
                                                        '<h4>' + shortTitle + '</h4>' +
                                                    '</div>' +
                                                '</a>';

                            // Render post
                            postRender = document.createElement('div');
                            postRender.setAttribute('class', 'post');
                            postRender.innerHTML = postMarkup;

                            slideItem.appendChild(postRender);
                        };                        

                        mediumWrapper.appendChild(swiperWrapper);
                        swiperWrapper.appendChild(slideItem);

                        return i === 2;
                    });

                    MEDIUM.slider(mediumWrapper);
                    mediumWrapper.appendChild(prevSlide);
                    mediumWrapper.appendChild(nextSlide);
                    mediumWrapper.appendChild(pagination);
                } else {
                    MEDIUM.error();
                }
            }
        };
        xhr.open('GET', url, true);
        xhr.responseType = 'json';
        xhr.send(null);
    },

    error: function() {
        console.log('Error, please reaload page');
    },

    slider: function(element) {
        let swiper = new Swiper(element, {
            navigation: {
                nextEl: nextSlide,
                prevEl: prevSlide,
            },
            pagination: {
                el: pagination,
                clickable: true,
            },            
            spaceBetween: 50,
            speed: 1000,
            on: {
                init: function () {                    
                    nextSlide.classList.add('active');
                },

                reachBeginning: function() {
                    nextSlide.classList.add('active');
                    prevSlide.classList.remove('active');
                },

                reachEnd: function() {                    
                    prevSlide.classList.add('active');
                    nextSlide.classList.remove('active');
                }
            }            
        });
    },

    init: function() {
        MEDIUM.posts();        
    }
};

module.exports = {
    init : MEDIUM.init
};
